## AtomNeb (IDL version)
[![GitHub license](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/equib/AtomNeb/blob/master/LICENSE)

**Atomic Data for Ionized Nebulae**


### Description

The **AtomNeb** database contains sets of [atomic datasets](https://github.com/equib/AtomNeb/tree/master/atomic-data), which were generated for use in [proEQUIB](https://github.com/equib/proEQUIB), [pyEQUIB](https://github.com/equib/pyEQUIB), and other nebular spectral analysis tools. They include energy levels (Ej), collision strengths (Ωij), and transition probabilities (Aij) of the most ions commonly observed in ionized nebulae.

The atomic datasets are as follows:

* **[Collection](https://github.com/equib/AtomNeb/tree/master/atomic-data/collection)** from the [National Institute of Standards and Technology (NIST) Atomic Spectra Database](https://www.nist.gov/pml/atomic-spectra-database), the [CHIANTI atomic database](http://www.chiantidatabase.org/), and some improved atomic data from Cloudy v13.04 and pyNeb v1.0.

* **[Chianti52](https://github.com/equib/AtomNeb/tree/master/atomic-data/chianti52)** from the [CHIANTI atomic database](http://www.chiantidatabase.org/) version 5.2.

* **[Chianti60](https://github.com/equib/AtomNeb/tree/master/atomic-data/chianti60)** from the [CHIANTI atomic database](http://www.chiantidatabase.org/) version 6.0.

* **[Chianti70](https://github.com/equib/AtomNeb/tree/master/atomic-data/chianti70)** from the [CHIANTI atomic database](http://www.chiantidatabase.org/) version 7.0.

Each dataset contains the following [atomic data FITS files](https://github.com/equib/AtomNeb/tree/master/atomic-data/chianti70):

* **AtomElj.fits** contains *Energy Levels* (Ej).

* **AtomOmij.fits** contains *Collision Strengths* (Ωij).

* **AtomAij.fits** contains *Transition Probabilities* (Aij).

Website: [physics.mq.edu.au/~ashkbiz/atomneb](http://physics.mq.edu.au/~ashkbiz/atomneb/)
